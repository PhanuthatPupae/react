import React from 'react';
import { Route } from 'react-router-dom';
import Loginpage from './login';
import Mainpage from './Main';

function Routes() {
  return (
    <div className="app">
      <Route exact path="/" component={Loginpage} />
      <Route exact pat h="/movies" component={Mainpage} />
    </div>
  );
}
export default Routes;
