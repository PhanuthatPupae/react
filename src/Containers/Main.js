import React, { Component } from "react";
import { Spin, Modal } from "antd";
import ListMovie from "../Components/ListMovie";

class Main extends Component {
  state = { items: [], isShowModal: false, itemMovie: null };

  onItemMovieClick = item => {
    this.setState({ isShowModal: true, itemMovie: item });
  };

  onModalclickOk = () => {
    this.setState({
      isShowModal: false
    });
  };

  onModalClickCancel = () => {
    this.setState({
      isShowModal: false
    });
  };

  componentDidMount() {
    fetch("https://workshopup.herokuapp.com/movie")
      .then(response => response.json())
      .then(movies => this.setState({ items: movies.results }));
  }

  render() {
    console.log("items: ", this.state.items);

    return (
      <div>
        {this.state.items.length > 0 ? (
          <ListMovie
            items={this.state.items}
            onItemMovieClick={this.onItemMovieClick}
          />
        ) : (
          <Spin size="large" />
        )}

        <Modal
          title="Basic Modal"
          visible={this.state.isShowModal}
          onOk={this.onModalclickOk}
          onCancel={this.onModalClickCancel}
        >
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
        </Modal>
      </div>
    );
  }
}

export default Main;
