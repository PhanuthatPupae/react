import React, { Component } from "react";
import { Form, Button, Input, Icon, message } from "antd";
import { withRouter } from "react-router-dom";

const KEY_USER_DATA = "user_data";
class login extends Component {
  state = {
    email: "",
    password: ""
  };

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push("/movies");
  };

  conponentDidMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      this.navigateToMainPage();  
    }
  }
  onEmail = event => {
    const email = event.target.value;

    // console.log(event.target.value);
    this.setState({ email });
  };

  onPassword = event => {
    const password = event.target.value;
    this.setState({ password });
  };
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  validatePassword(password) {
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    return re.test(String(password));
  }
  onSubmitFormLogin = e => {
    const { history } = this.props;
    e.preventDefault();
    const isValid = this.validateEmail(this.state.email);
    const isValidPassword = this.validatePassword(this.state.password);

    console.log(isValidPassword);
    console.log(isValid);

    if (isValid && isValidPassword) {
      history.push("/movies");

      localStorage.setItem(KEY_USER_DATA, JSON.stringify({ isLoggedIn: true }));
    } else {
      message.error("Email password invaid !", 1);
    }
  };
  render() {
    return (
      <div>
        <h2>Log In</h2>
        <Form onSubmit={this.onSubmitFormLogin}>
          <Form.Item>
            <Input
              prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder="Email"
              onChange={this.onEmail}
            />
          </Form.Item>
          <Form.Item>
            <Input
              prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
              type="password"
              placeholder="Password"
              onChange={this.onPassword}
            />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}
export default withRouter(login);
