import React, { Component } from 'react';


//import LoginPage from './Containers/login'
import './App.css';
import Routes from './Containers/route';
class App extends Component {
  render() {
    return (
      <div className="App">
        <Routes />
      </div>
    );
  }
}

export default App;
