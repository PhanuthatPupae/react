import React from "react";
import { Card, Icon, Avatar } from "antd";
import Item from "antd/lib/list/Item";
import TextTruncate from "react-text-truncate";
const { Meta } = Card;
function ItemMovie(props) {
  const item = props.item;
  return (
    <Card
      onClick={() => {
        props.onItemMovieClick(item);
      }}
      hoverable
      cover={<img alt="example" src={item.image_url} />}
    >
      <Meta
        title={Item.title}
        description={
          <TextTruncate
            line={1}
            truncateText="…"
            text={item.overview}
            textTruncateChild={<a href="#">Read on</a>}
          />
        }
      />
    </Card>
  );
}
export default ItemMovie;
